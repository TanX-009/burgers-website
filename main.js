import "./style.css";

const titleContainer = document.getElementById("titleContainer");
const burgerContainer = document.getElementById("burgerContainer");

function titleMove() {
  let mouseX = 0,
    mouseY = 0;
  let tX, tY, bX, bY;

  let titleX, titleY, burgerX, burgerY;
  titleX = titleContainer.offsetWidth / 2;
  titleY = titleContainer.offsetHeight / 2;
  burgerX = burgerContainer.offsetWidth / 2;
  burgerY = burgerContainer.offsetHeight / 2;

  titleContainer.children[0].style.top = titleY + "px";
  titleContainer.children[0].style.left = titleX + "px";
  burgerContainer.children[0].style.top = burgerY + "px";
  burgerContainer.children[0].style.left = burgerX + "px";

  window.addEventListener("resize", () => {
    titleX = titleContainer.offsetWidth / 2;
    titleY = titleContainer.offsetHeight / 2;
    burgerX = burgerContainer.offsetWidth / 2;
    burgerY = burgerContainer.offsetHeight / 2;

    titleContainer.children[0].style.top = titleY + "px";
    titleContainer.children[0].style.left = titleX + "px";
    burgerContainer.children[0].style.top = burgerY + "px";
    burgerContainer.children[0].style.left = burgerX + "px";
  });

  tX = titleX;
  tY = titleY;
  bX = burgerX;
  bY = burgerY;

  document.addEventListener("mousemove", (event) => {
    mouseX =
      (event.clientX * 90 - titleContainer.offsetWidth / 2) /
      titleContainer.offsetWidth /
      2;
    mouseY =
      (event.clientY * 90 - titleContainer.offsetHeight / 2) /
      titleContainer.offsetHeight /
      2;

    tX += mouseX + titleX - tX;
    tY += mouseY + titleY - tY;

    titleContainer.children[0].style.top = tY + "px";
    titleContainer.children[0].style.left = tX + "px";

    bX += -mouseX + burgerX - bX;
    bY += -mouseY + burgerY - bY;

    burgerContainer.children[0].style.top = bY + "px";
    burgerContainer.children[0].style.left = bX + "px";
    console.log(tX, tY);
  });
}
titleMove();
